# Base Server

Base Server es un servidor para aplicaciones web estáticas.

### Tecnologías utilizadas

- EXPRESS, framework de desarrollo para el backend.
- EJS, como motor de plantillas.

### Instalación

Para instalar el proyecto ver el archivo
 [INSTALL.md](https://gitlab.com/waquispe/base-server/blob/master/INSTALL.md).
